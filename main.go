package main

import (
	"flag"
	"net/http"
	"os/exec"

	"gitee.com/zzoe/httplog/backend/routes"
	"golang.org/x/text/encoding"
	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/encoding/unicode"
)

func main() {
	var fencStr = flag.String("fenc", "UTF8", "FileEncoding: GB18030/GBK/UTF8(Default)")
	var fenc encoding.Encoding

	flag.Parse()

	switch *fencStr {
	case "GB18030":
		fenc = simplifiedchinese.GB18030
	case "GBK":
		fenc = simplifiedchinese.GBK
	default:
		fenc = unicode.UTF8
	}

	routes.RegistRouter(fenc)

	url := "http://127.0.0.1:21188/"
	exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()

	http.ListenAndServe(":21188", nil)
}
