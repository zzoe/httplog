import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'

import App from './App.vue'
import routes from './router.js'

Vue.use(VueRouter)
Vue.use(ElementUI)
Vue.prototype.$ajax = axios

// 创建 router 实例，然后传 `routes` 配置
const router = new VueRouter({
  // mode: 'history',
  routes: routes // （缩写）相当于 routes: routes
})

// 创建实例。
// 记得要通过 router 配置参数注入路由，
// 从而让整个应用都有路由功能
const app = new Vue({
  router,
  el: '#app',
  render: h => h(App)
})
