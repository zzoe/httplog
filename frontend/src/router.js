import log from './components/log.vue'
import set from './components/set.vue'
import about from './components/about.vue'

// 定义路由
const routes = [
  { path: '/log', component: log },
  { path: '/set', component: set },
  { path: '/about', component: about },
  { path: '', component: about }
]

export default routes
