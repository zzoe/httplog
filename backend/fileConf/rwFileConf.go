package fileConf

import (
	"io/ioutil"
	"log"
	"os"
	"sync"

	"github.com/golang/protobuf/proto"
)

// LogConfOperInfo 更新日志文件配置信息
type LogConfOperInfo struct {
	ConfMap map[string]*LogConfInfo
	RspChan chan string
}

var (
	// CurLogConfMap 当前日志参数配置
	CurLogConfMap sync.Map
	// ConfOperChan 新增/修改/删除配置信息的通道
	ConfOperChan chan *LogConfOperInfo
)

// init 初始化日志配置信息文件
func init() {
	filename := "logSetting"

	ConfOperChan = make(chan *LogConfOperInfo)

	go setLogConf()

	in, err := ioutil.ReadFile(filename)
	if err != nil {
		recover()
		log.Println("Reading file ["+filename+"] err: ", err)
		initData()
		return
	}

	var initLogConf = &LogConf{map[string]*LogConfInfo{}}
	if err := proto.Unmarshal(in, initLogConf); err != nil {
		log.Println("Failed to parse file ["+filename+"]:", err)
		initData()
		return
	}

	for k, v := range initLogConf.ConfMap {
		CurLogConfMap.Store(k, v)
	}

	return
}

func initData() {
	log.Println("Init file now...")
	confMap := map[string]*LogConfInfo{}
	confMap["amsbp"] = &LogConfInfo{
		Value: "amsbp",
		Label: "Ams KCBP",
		Path:  "E:/039-CSNSHZG/src/KCBP/ams/log/run/<date>/runlog.log",
		Rows:  666}

	confMap["pubbp"] = &LogConfInfo{
		Value: "pubbp",
		Label: "Pub KCBP",
		Path:  "E:/039-CSNSHZG/src/KCBP/pub/log/run/<date>/runlog.log",
		Rows:  666}

	confMap["gate"] = &LogConfInfo{
		Value: "gate",
		Label: "HttpGate",
		Path:  "E:/039-CSNSHZG/src/Gate/log/gateLog.<date>.000.log",
		Rows:  666}

	var rspChan chan string
	rspChan = make(chan string)
	defer close(rspChan)
	ConfOperChan <- &LogConfOperInfo{ConfMap: confMap, RspChan: rspChan}
	log.Println(<-rspChan)
}

// setLogConf 添加/更新/删除配置
func setLogConf() {
Ready:
	confOper := <-ConfOperChan
	for k, v := range confOper.ConfMap {
		CurLogConfMap.Store(k, v)
	}

	confOper.RspChan <- "更新成功！"
	store()

	goto Ready
}

// store Write log Settings to file.
func store() {
	var curLogConf = &LogConf{map[string]*LogConfInfo{}}
	CurLogConfMap.Range(func(k, v interface{}) bool {
		curLogConf.ConfMap[k.(string)] = v.(*LogConfInfo)
		return true
	})
	out, err := proto.Marshal(curLogConf)
	if err != nil {
		log.Fatalln("Failed to encode logConf:", err)
	}

	if err := ioutil.WriteFile("logSetting.new", out, 0644); err != nil {
		log.Println(curLogConf.String())
		log.Fatalln("Failed to write file logSetting:", err)
	}
	os.Rename("logSetting.new", "logSetting")
}
