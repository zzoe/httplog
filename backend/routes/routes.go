package routes

import (
	"net/http"

	"gitee.com/zzoe/httplog/backend/logSetHandler"
	"gitee.com/zzoe/httplog/backend/logViewHandler"
	"golang.org/x/text/encoding"
)

// RegistRouter 路由
func RegistRouter(fileEncoding encoding.Encoding) {
	logViewHandler.Fenc = fileEncoding
	http.HandleFunc("/logView", logViewHandler.Get)

	http.HandleFunc("/logSet", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			logSetHandler.Get(w, r)
		case http.MethodPost:
			logSetHandler.Post(w, r)
		}
	})

	http.HandleFunc("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "static/icon/favicon.ico")
	})

	http.Handle("/", http.FileServer(http.Dir("static/dist/")))
}
