package logSetHandler

import (
	"encoding/json"
	"log"
	"net/http"

	"gitee.com/zzoe/httplog/backend/fileConf"
)

type logConfig struct {
	itemName  string
	itemValue string
	viewName  string
}

// Get 响应/logSet Get请求,设置日志路径
func Get(w http.ResponseWriter, r *http.Request) {
	confArr := make([]*fileConf.LogConfInfo, 0)
	fileConf.CurLogConfMap.Range(func(k, v interface{}) bool {
		confArr = append(confArr, v.(*fileConf.LogConfInfo))
		return true
	})
	//w.Write([]byte(confArr))
	json.NewEncoder(w).Encode(confArr)
}

func chkErr(w http.ResponseWriter, err error) {
	if err != nil {
		log.Println(err)
		w.Write([]byte(err.Error()))
		return
		// log.Fatal(err)
	}
}
