package logSetHandler

import (
	"encoding/json"
	"log"
	"net/http"

	"gitee.com/zzoe/httplog/backend/fileConf"
)

type postData struct {
	ModMap map[string]*fileConf.LogConfInfo `json:"modMap"`
}

// Post 响应/logSet Post请求,设置日志路径
func Post(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	var t postData
	if err := decoder.Decode(&t); err != nil {
		// panic(err)
		http.NotFound(w, r)
	}
	log.Println(t.ModMap)

	var rspChan chan string
	rspChan = make(chan string)
	defer close(rspChan)
	fileConf.ConfOperChan <- &fileConf.LogConfOperInfo{ConfMap: t.ModMap, RspChan: rspChan}
	log.Println(<-rspChan)
}
